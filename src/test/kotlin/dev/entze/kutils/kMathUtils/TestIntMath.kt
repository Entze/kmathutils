package dev.entze.kutils.kMathUtils

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

internal class TestModulo {

    @Test
    fun test_modulo_positive_positive() {
        assertEquals(2, 7.modulo(5))
    }

    @Test
    fun test_modulo_negative_positive() {
        assertEquals(3, (-7).modulo(5))
    }

    @Test
    fun test_modulo_positive_negative() {
        assertEquals(-3, 7.modulo(-5))
    }

    @Test
    fun test_modulo_negative_negative() {
        assertEquals(-2, (-7).modulo(-5))
    }

    @Test
    fun error_modulo_zero() {
        assertFailsWith(ArithmeticException::class) {
            5.modulo(0)
        }
    }

}

