package dev.entze.kutils.kMathUtils.fraction

class IntFraction private constructor(val numerator: Int, val denominator: Int) : Number(), Comparable<IntFraction> {

    operator fun unaryPlus(): IntFraction {

        TODO("Not yet implemented")
    }

    operator fun unaryMinus(): IntFraction {

        TODO("Not yet implemented")
    }

    operator fun plus(other: Byte): IntFraction {

        TODO("Not yet implemented")
    }

    operator fun plus(other: Short): IntFraction {

        TODO("Not yet implemented")
    }

    operator fun plus(other: Int): IntFraction {

        TODO("Not yet implemented")
    }

    operator fun plus(other: Long): LongFraction {

        TODO("Not yet implemented")
    }

    operator fun plus(other: Float): Float {

        TODO("Not yet implemented")
    }

    operator fun plus(other: Double): Double {

        TODO("Not yet implemented")
    }

    operator fun plus(other: ByteFraction): IntFraction {

        TODO("Not yet implemented")
    }


    override fun compareTo(other: IntFraction): Int {
        TODO("Not yet implemented")
    }

    //abstract fun reduce(): Unit

    override fun toByte(): Byte = (numerator / denominator).toByte()


    override fun toChar(): Char {
        TODO("Not yet implemented")
    }

    override fun toDouble(): Double {
        TODO("Not yet implemented")
    }

    override fun toFloat(): Float {
        TODO("Not yet implemented")
    }

    override fun toInt(): Int {
        TODO("Not yet implemented")
    }

    override fun toLong(): Long {
        TODO("Not yet implemented")
    }

    override fun toShort(): Short {
        TODO("Not yet implemented")
    }

    override fun toString(): String = if (denominator == 1) {
        numerator.toString()
    } else {
        "$numerator/$denominator"
    }

}